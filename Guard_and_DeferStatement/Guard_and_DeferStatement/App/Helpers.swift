import SwiftUI

// MARK: -∂ Custom colors
struct MyColors {
    //__________
    static let netflixRed = Color.init(red: 229/255, green: 10/255, blue: 21/255)
}

/**©--------------------------------------------------------------©*/

/** [ HOW TO CALL ]
 e.add.apply(to: 10, and: 7)
 e.sub.apply(to: 10, and: 7)
 e.mult.apply(to: 10, and: 7)
 e.div.apply(to: 10, and: 2) */
typealias e = ECalcOperation
enum ECalcOperation {
    case add, sub, mult, div
    
    func apply(to arg1: Int, and arg2: Int) {
        //∆..........
        switch self {
        case .add: print("*. \(arg1) + \(arg2) = \(arg1 + arg2)")
        case .sub: print("*. \(arg1) - \(arg2) = \(arg1 - arg2)")
        case .mult: print("*. \(arg1) * \(arg2) = \(arg1 * arg2)")
        case .div: print("*. \(arg1) / \(arg2) = \(arg1 / arg2)")
        }
    }
}

/** [ HOW TO CALL ]
 let url = "https//:Testing.com"
 print(url.asURL ?? "")
 -----<< OR >>-----
 guard let url = "https//:Testing.com".asURL else { return }
 print(url)
 */
extension String {
    var asURL: URL? {
        URL(string: self)
    }
}

extension Int {
    func toDouble() -> Double {
        Double(self)
    }
}

extension Double {
    func toInt() -> Int {
        Int(self)
    }
}

extension Int {
    func toCGFloat() -> CGFloat {
        CGFloat(self)
    }
}

// MARK: -∂ Provides a formatted currency in dollars "$123.44"
func currency(price: Double) -> String {
    //__________
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    /// # formatter.locale = NSLocale.currentLocale()
    /// # This is the defaultIn Swift 4, this ^ has
    /// # been renamed to simply NSLocale.current
    guard let result = formatter.string(from: NSNumber(value: price))
    else { return "" }
    
    return result
}

// MARK: -∂ Provides a formatted currency in dollars "$123.44"
// MARK: but NumberFormatter() is suggested to be used instead
func convertToCash(price: Double) -> String {
    //..........
    String(format: "$%.02f", price)
}

// MARK: -∂ A starter placeholder for anything you want
func myPlaceHolder(myStr: String) -> some View {
    Text("➡ \(myStr) 🎯!⬅")
        .font(.system(size: 22))
        .foregroundColor(.white)
        .bold()
        .background(Color.black)
}
// MARK: -∂ arrayToString(): Prints out a formatted array
func arrayToStr(arrayName: String = "", array: [String]) {
    //__________
    let joined = array.joined(separator: " | ")
    print("\(arrayName): \(joined)")
}

// MARK: -∂ A function that handles 2d
func extract2D(strMsg: String = "", index: Int, itemIndex: Int, _ array2D: [[String]]) {
    //__________
    let indexes = array2D[index][itemIndex]
    print("\(strMsg): \(indexes.description)")
}

@discardableResult func divT(_ str: String) -> String {
    let myStr = """
                .............................................................
                                \(str)
                .............................................................
                """
    print(myStr)
    return myStr
}

@discardableResult func logT<T>(str: String, obj_t: T) -> String {
    let myStr = """
                |>==================================================
                |> \(str) \(obj_t)
                |>==================================================
                """
    print(myStr)
    return myStr
}

// A generic closure, that prints 'Any' tye of object
var printf : (Any) -> Void = { arg in
    let myStr = """
                ....................................................
                ∆. \(arg)
                ....................................................
                """
    print(myStr)
}

// Great place holder function kinda a phantom type
func undefined<T>(_ msg: String = "") -> T {
    fatalError("Undefined: \(msg)")
}

// Given a value to round and a factor to round to,
// round the value to the nearest multiple of that factor.
/*-------------------------------------------------------
 EXAMPLE USAGE:
 tipPctLabel.text =
 "Tip: \(roundTo(Double(tipPctSlider.value), toNearest: 0.20))"
 -------------------------------------------------------*/
func roundToPlace(value: Double, decimalPlaces: String = "%0.02f") -> Double {
    let result = String(format: decimalPlaces, value)
    return Double(result) ?? 0.0
}

// A closure, that Squares² a number. NOTE--! Has typealias
typealias numInNumOut = (Double) -> Double
var imASquare: numInNumOut = { numero  in
    
    let result = numero * numero
    print("| \(numero)² = \(roundToPlace(value: result))")
    
    return result
}

/** A function that filters numbers divisible by 2 */
func filterDiv2(args: [Int]) {
    let oddNums: [Int] = (args).filter { $0 % 2 == 0 }
    // Needs logT<--?
    logT(str: "Numbers divisible by '2': ", obj_t: oddNums)
}

/** A function that filters numbers divisible by 3 */
func filterDiv3(args: [Int]) {
    let oddNums: [Int] = (args).filter { $0 % 3 == 0 }
    // needs logT<--?
    logT(str: "Numbers divisible by '3': ", obj_t: oddNums)
}

/** A function that outputs the current date as: [Month Day, Year]
 In --> String format, not a date object
 but requires a singleton instance of date to be created */
@discardableResult func fixDate() -> String {
    let formatter = DateFormatter()
    let currentDate = Date()
    formatter.dateStyle = .medium
    let result = formatter.string(from: currentDate)
    return result
}

/** A function that multiplies an array of any size & any numeric type */
func multiplyList<Number: Numeric>(multiplier: Number, numbers: [Number]) {
    //__________
    let total = numbers.reduce(multiplier, *)
    print("1. count<Number: Numeric>: \(total)")
}

// Returns current date formatted, once a date object is passed
func currentDate(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .none
    
    // US English Locale (en_US)
    dateFormatter.locale = Locale(identifier: "en_US")
    let todaysDate = dateFormatter.string(from: date)
    print(todaysDate)
    
    return todaysDate
}

/** A function that only returns even numbers */
func evenNums(num1: Int, num2: Int) {
    let oddNums: [Int] = (num1 ... num2).filter { $0 % 2 == 0 }
    logT(str: "Even number", obj_t: oddNums)
}

/** A function that only returns odd numbers */
func oddNums(num1: Int, num2: Int) {
    let oddNums: [Int] = (num1 ... num2).filter { $0 % 3 == 0 }
    logT(str: "Odd number", obj_t: oddNums)
}

/*©-----------------------------------------©*/
extension Date {
    
    /**
     Sets Formatting for out dates. Returns a String
     date.dateRightNow()// Apr 26, 2020<--Example
     ## Important Notes ##
     1. This extends a Date; so it is usable on all Date objects
     2. Currently Set to medium Date length
     */
    func dateRightNow() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
}
/**©------------------------------------------------------------------------------©*/





