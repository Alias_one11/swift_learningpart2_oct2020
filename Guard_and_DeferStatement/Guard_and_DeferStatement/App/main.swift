import Foundation

func main() -> Void
{
    print("\n\n[ Guard Statement ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1
    let iCanDrink = false
    //∆..........
    checkDrinkingAge(canDrink: iCanDrink)
    
    //*©-----------------------------------------------------------©*/
    /// - ©Example #2
    let publicName: String? = "Alias111"
    let publicPhoto: String? = "Alias111 🧏🏾"
    let publicAge: Int? = 20
    
    unwrapWithGuard(strMsg: "Public Name", unwrapMe: publicName)
    unwrapWithGuard(strMsg: "Public Photo", unwrapMe: publicPhoto)
    unwrapWithGuard(unwrapMe: publicAge)
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()

func checkDrinkingAge(canDrink: Bool) -> Void {
    //∆..........
    guard canDrink else { return print("∆- [CANNOT DRINK]..not of drinking age.") }
}

func unwrapWithGuard<T>(strMsg: String = "", unwrapMe: T?) -> Void {
    //∆..........
    guard let result = unwrapMe else { return printf("∆- Could not unwrap optional") }
    /// ∆ - REPLACE CUSTOM `printf closure` with
    /// normal `print()` if `printf` is unvailable
    strMsg == "" ? print("∆- \(result)") : print("∆- \(strMsg): \(result)")
}
