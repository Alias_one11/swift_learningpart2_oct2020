import Foundation

class Human {
    // MARK: - ©Global-PROPERTIES
    //∆..............................
    var name: String
    //∆..............................
    
    
    /// ∆ Initializer
    //∆.................................
    init(name: String) { self.name = name }
    
    /// ∆ Convenience init
    convenience init() {
        self.init(name: "1∆. convenience init()")
    }
    
    //∆.................................
}
