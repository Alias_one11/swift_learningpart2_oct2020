import Foundation

func main() -> Void
{
    print("\n\n[ OOP ]")
    print("-----------------------------------------------------------")
    printf("- ©Example #1 convenience init")
    
    let james = Human()
    // Using convenience init.self--> set to a empty string ""
    print("\(james.name)\n")
    
    printf("- ©Example #1 computed-properties")
    
    var radius: Double = 10
    
    /// ∆ Read-Only
    var diameter: Double {
        get { radius * 2 }
    }
    
    print("2∆. Read-Only Diameter: \(diameter)")
    /// diameter = 30 // ERROR read-only property
    
    /// ∆ Read & Write
    var diameter2: Double {
        get { radius / 2 }
        set { radius = newValue }
        // ∆ Can also be set(value) { radius = value }
    }
    
    diameter2 = 34
    print("3∆. Read & Write Diameter: \(diameter2)\n")
    //*©-----------------------------------------------------------©*/
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
