import Foundation

/// ∆ @main-------------------------------------------------------------
print("\n\n[ Singleton-Pattern(Shared_Instance) ]")
print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞")
logT("- ©Example #1")

/// ∆ Only creates the shared instance once it is accessed
logT("Acct-Mgr ID: { \(AccountManager.shared.userInfo.ID) }")

/// ∆ Reusing the shared instance multiple times but
///   instatiated within the class once ☝🏾
AccountManager.shared.userInfo.ID = "Jesse"
logT("New Acct-Mgr ID: { \(AccountManager.shared.userInfo.ID) }")


//*©-----------------------------------------------------------©*/

print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞\n")

