import Foundation

/// ∆ A singleton is a object that only gets instantiated once
class AccountManager {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    static var shared = AccountManager()
    var userInfo = (ID: "Alias The Dev", Password: 1927349201)
    //∆..............................
    
    /// ∆ private init can only be instantiated in this class
    //∆.................................
    private init() {
        logT("AccountManager instance created 🤖")
    }
    //∆.................................
    
    ///∆ ........... Class Methods ...........
    func network() -> Void {
        //∆..........
        
    }
}
