import Foundation

func main() -> Void
{
    print("\n\n[ OOP #2 ]")
    print("-----------------------------------------------------------")
    printf("- ©Example #1 property observers/ tracking a property")
    
    /// ∆ [willSet]: is called just before the value is stored.
    /// ∆ [didSet]: is called immediately after the new value has been stored.
    var myGrade: Int = 80 {
        //∆ Defaults BUILT-IN: You can also just used
        //∆ willSet-->newValue didSet-->oldValue
        willSet(newGrade) { print("∆. My grade will set to: \(newGrade)%") }
        didSet(oldGrade) { print("∆. My grade did set to: \(oldGrade)%") }
    }
    
    myGrade = 100
    _ = myGrade
    
    print()
    //*©-----------------------------------------------------------©*/
    printf("- ©Example #2 property observers/ tracking a property")
    
    var totalSteps: Int = 20 {
        //∆..........
        willSet(newTotalSteps) { print("∆. About to set total steps to: \(newTotalSteps)") }
        //∆..........
        didSet(oldTotalSteps) {
            totalSteps > oldTotalSteps
                ? print("""
                        ˆ∆. Orignal step count: \(oldTotalSteps)
                        Added { \(totalSteps - oldTotalSteps) } steps
                        """)
                : print("∆. No steps added: \(oldTotalSteps)")
        }
    }
    
    totalSteps = 50
    _ = totalSteps
    
    print()
    //*©-----------------------------------------------------------©*/
    printf("- ©Example #3 property observers/ tracking a property")
    
    var isUserLoggedIn: Bool = false {
        willSet {
            print("∆. The user has tried something...")
        }
        //∆..........
        didSet {
            /// ∆ Once the user is logged in:
            /// ∆ You can, for example
            /// ∆ : Change background color
            ///   : Switch to another view
            ///   : Alert pop up
            isUserLoggedIn
                ? print("∆. The user has switched from logged in: " +
                        "\(oldValue) to \(isUserLoggedIn)")
                : print(oldValue)
        }
    }
    
    isUserLoggedIn = true
    _ = isUserLoggedIn
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
