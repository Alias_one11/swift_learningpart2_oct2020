import Foundation

/// ∆ @main-------------------------------------------------------------
print("\n\n[ Failable Init ]")
print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞")
printf("- ©Example #1")

let aliasTheDev = Blog(name: "")
logT("Did the failable init go off: ", obj_t: aliasTheDev)

let aliasTheDataScientist = Blog(name: "Alias the great Dev!")
let theCoolDev = aliasTheDataScientist?.name ?? ""
logT("Did the failable init go off: ", obj_t: theCoolDev)


//*©-----------------------------------------------------------©*/

print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞\n")

