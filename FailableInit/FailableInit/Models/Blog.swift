import SwiftUI

class Blog {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    let name: String
    //∆..............................
    
    /// ∆ Failable initializer
    //∆.................................
    init?(name: String) {
        if name.isEmpty {
            return nil
        } else { self.name = name }
    }
    //∆.................................
}
