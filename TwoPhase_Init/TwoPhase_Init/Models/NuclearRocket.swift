import Foundation

import SwiftUI

struct NuclearRocket {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    var meters, liters: Double
    //∆..............................
    
    /// ∆ Two Phase Initializer
    //∆.................................
    /// ∆ init for the British 🇬🇧
    init(meters: Double, liters: Double) {
        self.meters = meters
        self.liters = liters
    }
    
    /// ∆ init for the USA 🇺🇸
    init(ft: Double, gallons: Double) {
        //∆..........
        let convertedMeters = ft / 3.28
        let convertedLiters = gallons * 3.78
        
        self.init(meters: convertedMeters, liters: convertedLiters)
    }
    //∆.................................
}
