import Foundation

/// ∆ @main-------------------------------------------------------------
print("\n\n[ Two Phase Initialization ]")
print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞")
logT("- ©Example #1")

var rocket = NuclearRocket(meters: 20, liters: 20)
logT("British 🇬🇧 metric system\n" +
     " «. Meters: \(rocket.meters) | Liters: \(rocket.liters)")

var newRocket = NuclearRocket(ft: 20, gallons: 20)
logT("USA 🇺🇸 metric system\n" +
        " «. Ft: \(String(format: "%.2f",newRocket.meters)) | Gallons: \(rocket.liters)")


//*©-----------------------------------------------------------©*/

print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞\n")

