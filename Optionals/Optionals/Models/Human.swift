import SwiftUI

class Human {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    var name: String
    //∆..............................
    
    /// # Initializer
    //∆...................................................
    init(name: String) {
        self.name = name
    }
    //∆...................................................
    
    func sayHello() {
        //∆..........
        printf("Hello, im \(name)")
    }
}/// ∆ END HUMAN

//∆.....................................................

class Apartment {
    // MARK: - ©Global-PROPERTIES
    //∆..............................
    var human: Human? = nil
    //∆..............................
}

