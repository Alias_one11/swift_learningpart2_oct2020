import Foundation

func main() -> Void
{
    print("\n\n[ Optionals ]")
    print("-----------------------------------------------------------")
    print("©Example #1 Introduction to optionals. print out with default")
    
    let aliasApartments = Apartment()
    aliasApartments.human = Human(name: "Alias111")
    
    aliasApartments.human?.sayHello()// Could be nil if no human instance exist
    printf("Return: \(aliasApartments.human?.name ?? "[NO VALUE]")<--NOT nil")
    
    // ∆ Will return nil? since name is never set with  value
    let chestersApartments = Apartment()
    printf("Return: \(chestersApartments.human?.name ?? "[NO VALUE]")<--nil FOR SURE!!!")
    
    //*©-----------------------------------------------------------©*/
    print("\n©Example #2 Implicit & Explicit unwrapping a optional")
    
    let nycApartments = Apartment()
    nycApartments.human = Human(name: "Jsin311")
    
    /// ∆ Implicit
    if let residentJsin311 = nycApartments.human?.name {
        printf("Residents Name: \(residentJsin311)")
        //∆..........
    } else { printf("[ERROR]..no resident with that name...") }
    
    /// ∆ Explicit
    // let palaceApartments = Apartment()
    // printf("Residents Name: \(palaceApartments.human!.name)")
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
