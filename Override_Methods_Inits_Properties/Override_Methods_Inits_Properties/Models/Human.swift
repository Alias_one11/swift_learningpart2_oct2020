import Foundation

class Human {
    // MARK: - ©Global-PROPERTIES
    //∆..............................
    var origin: String
    //∆..............................
    
    /// ∆ Initializer
    //∆.................................
    init(enterOrigin: String) {
        origin = enterOrigin
    }
    //∆.................................
}

class Heritage: Human {
    // MARK: - ©Global-PROPERTIES
    //∆..............................
    var city: String
    //∆..............................
    
    /// ∆ Overriding PARENT classes & using super.init
    //∆.................................
    init(enterCity: String) {
        city = enterCity
        super.init(enterOrigin: "Puerto Rico 🇵🇷")
    }
    
    /// ∆ Overloading init
    init(enterCity: String, origin: String) {
        city = enterCity
        super.init(enterOrigin: origin)
    }
    //∆.................................
    
    ///∆ ........... toString Method ...........
    @discardableResult func toStr() -> String {
        //∆..........
        logT("Country Of Origin: { \(super.origin) }\n«. From City: { \(city) }")
    }
    
}
