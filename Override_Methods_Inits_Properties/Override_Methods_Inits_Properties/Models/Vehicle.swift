import Foundation

class Vehicle {
    // MARK: - ©Computed-PROPERTIES
    //∆..............................
    var description: String {
        return "Hello, I'm moving at 30km/hr"
    }
    //∆..............................
    
    /// ∆ Initializer
    //∆.................................
    
    //∆.................................
    
    ///∆ ............... Class Methods ...............
    func warning() {
        //∆..........
        print("«. Be careful, I'm going pretty fast!..")
    }
}/// ∆ END VEHICLE

//∆.....................................................
/// ∆ Overriding Methods & Properties(Inheritance)

class ToyCar: Vehicle {
    // MARK: - ©Global-PROPERTIES
    /// `super` keyword uses the old value overriding from
    //∆..............................
    override var description: String {
        return "\(super.description), but im about to speed up!.."
    }
    //∆..............................
    
    ///∆ ............... Overriding Methods ...............
    override func warning() {
        logT("Please dont mind me!")
        super.warning()
    }
}

