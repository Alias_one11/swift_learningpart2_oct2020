import Foundation

class Tesla {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    var numberOfWheels: Int
    //∆..............................
    
    /// ∆ Initializer
    //∆.................................
    init(enterNumberOfWheels: Int) {
        numberOfWheels = enterNumberOfWheels
    }
    //∆.................................
}
//∆.....................................................

/// ∆ Overriding init
class ModelS: Tesla {
    /// ∆ Initializer
    //∆.................................
    override init(enterNumberOfWheels numberOfWheels: Int) {
        //∆..........
        super.init(enterNumberOfWheels: numberOfWheels)
        logT("Wow 🤩 this is a beautiful vehicle!..")
    }
    //∆.................................
    
    ///∆ ........... toString Method ...........
    @discardableResult func toStr() -> String {
        //∆..........
        let result = "Number of wheels on Model-S: \(super.numberOfWheels)"
        logT(result)
        
        return result
    }
}
