import Foundation

/// ∆ @main-------------------------------------------------------------
print("\n\n[ Override Methods-Initializers-Properties ]")
print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞")
logT("- ©Example #1")

let aliasToyCar = ToyCar()
logT(aliasToyCar.description)
aliasToyCar.warning()

print("\n")
//*©-----------------------------------------------------------©*/
logT("- ©Example #2")

let countryOfOrigin = Human(enterOrigin: "Puerto Rico 🇵🇷")

/// ∆ Overriding the initializer with super.init()
let alias = Heritage(enterCity: "Rio Piedra")
alias.toStr()

/// ∆ Overloaded super.init
let jesse = Heritage(enterCity: "Higuey", origin: "Dominican Republic 🇩🇴")
jesse.toStr()

print("\n")
//*©-----------------------------------------------------------©*/
logT("- ©Example #3")

let futureCar = ModelS(enterNumberOfWheels: 17)
futureCar.toStr()

print("∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞\n")

